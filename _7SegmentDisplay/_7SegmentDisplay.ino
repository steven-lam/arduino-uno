int data [] = {0,1,2,3,4,5,6};
int select [] = {7,8,9,10};
int dots = 11; 

void setup() {
  for(int i = 0; i < 7; i++)
    pinMode(data[i],OUTPUT);
  for (int j = 0; j < 4; j++)
    pinMode(select[j],OUTPUT);
  pinMode(dots, OUTPUT);
}


void loop () {
    for (int i = 0; i < 4; i++)
    digitalWrite(select[i],LOW);
    delay(2000
    );
  set(1);
  one();
  delay(1000);
  set(2);
  one();
  delay(1000);
  set(3);
  one();
  delay(1000);
  set(4);
  one();
  delay(1000);
}

void set(int choice) {
  for (int i = 0; i < 4; i++) 
    digitalWrite(select[i],LOW);
  switch(choice) {
    case 1: digitalWrite(select[0],HIGH);
    break;
    case 2: digitalWrite(select[1],HIGH);
    break;
    case 3: digitalWrite(select[2],HIGH);
    break;
    case 4: digitalWrite(select[3],HIGH);
    break;
    default: digitalWrite(select[0], HIGH); // Do nothing
  }
}

void one (void) {
  for(int i = 0; i<7; i++)
    digitalWrite(data[i],HIGH);
  digitalWrite(data[1],LOW);
  digitalWrite(data[2],LOW);
}
